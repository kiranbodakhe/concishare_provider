
var baseURL = "https://ef02b05e.ngrok.io";

angular.module('concishare_provider', ['ionic', 'ui.mask'])

  .config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html'
    })
    .state('app.main', {
      url: '/main',
      views: {
        'menuContent': {
          templateUrl: 'templates/main.html',
          controller: 'mainCtrl'
        }
      }
    })
    .state('app.login', {
      url: '/login',
      views: {
        'menuContent': {
          templateUrl: 'templates/login.html',
          controller: 'loginCtrl'
        }
      }
    })
    .state('app.forgotPassword', {
    url: '/forgotpassword',
      views: {
        'menuContent': {
          templateUrl: 'templates/forgotPassword.html',
          controller: 'forgotPasswdCtrl'
        }
      }
    })
    .state('app.resetPassword', {
      url: '/resetpassword',
      views: {
        'menuContent': {
          templateUrl: 'templates/resetPassword.html',
          controller: 'resetPasswordCtrl'
        }
      }
    })
    .state('app.signup', {
      url: '/signupstep1',
      views: {
        'menuContent': {
          templateUrl: 'templates/signup.html',
          controller: 'signUpCtrl'
        }
      }
    })
    .state('app.homeUnderProcess', {
      url: '/homeUnderProcess',
      views: {
        'menuContent': {
          templateUrl: 'templates/Home.html',
        }
      }
    });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/main');
})

  .directive('stringDirective', function() {
    return {
      restrict: 'E',
      scope: {
        field: "=",
        formModel: "=",
        fieldModel: "="
      },
      templateUrl: 'directives/string_directive.html'
    };
  })
  .directive('phoneDirective', function() {
    return {
      restrict: 'E',
      scope: {
        field: "=",
        formModel: "=",
        fieldModel: "="
      },
      templateUrl: 'directives/phone_directive.html'
    };
  })
  .directive('emailDirective', function() {
    return {
      restrict: 'E',
      scope: {
        field: "=",
        formModel: "=",
        fieldModel: "="
      },
      templateUrl: 'directives/email_directive.html'
    };
  })
  .directive('passwordDirective', function() {
    return {
      restrict: 'E',
      scope: {
        field: "=",
        formModel: "=",
        fieldModel: "="
      },
      templateUrl: 'directives/password_directive.html'
    };
  })
  .directive('address1Directive', function() {
    return {
      restrict: 'E',
      scope: {
        field: "=",
        formModel: "=",
        fieldModel: "="
      },
      templateUrl: 'directives/address_directive.html'
    };
  })
  .directive('zipDirective', function() {
    return {
      restrict: 'E',
      scope: {
        field: "=",
        formModel: "=",
        fieldModel: "="
      },
      templateUrl: 'directives/zip_directive.html'
    };
  })
  .directive('experienceDirective', function() {
    return {
      restrict: 'E',
      scope: {
        field: "=",
        formModel: "=",
        fieldModel: "="
      },
      templateUrl: 'directives/experience_directive.html'
    };
  })
  .directive('descriptionDirective', function() {
    return {
      restrict: 'E',
      scope: {
        field: "=",
        formModel: "=",
        fieldModel: "="
      },
      templateUrl: 'directives/description_directive.html'
    }
  });

//to-do -not working on this version
//will implement later

 /* .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if(ionic.Platform.isAndroid()) {
        console.log("inside android platform check");
        $ionicPlatform.registerBackButtonAction(function (event) {
          // hwBackButton_Pressed();
          event.preventDefault();
          event.stopPropagation();
          console.log("back button pressed inside registerBackButtonAction");
          return false;
        }, 100);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  });

*/
