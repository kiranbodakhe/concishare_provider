angular.module('concishare_provider')
  .controller('loginCtrl', function ($scope, $http, $location, $ionicPopup) {

    $scope.baseForm = {'fields':
      [
        { "fieldId": "phone",
          "name": "Phone Number *",
          "required": "true",
          "type": "string",
          "options": { "debounce": 0 }
        },
        { "fieldId": "password",
          "name": "Password *",
          "required": "true",
          "type": "string",
          "options": { "debounce": 0 }
        }
      ]
    };

    $scope.loginDetails  = {

      phone:"",
      password:""
    };

    $scope.login = function (loginDetails,loginForm) {

      if(loginForm.$valid){

        var loginData = {

          phoneNumber: loginDetails.phone,
          password:    loginDetails.password

        };
         $http.post(env.devURl + '/papi/authenticate', data = loginData).success(function(data, status, headers, config) {
         console.log('authenticated.',data.message, "status",status,"headers",headers,"config",config);
         $location.path('/app/homeUnderProcess');
         }).error(function(error) {
           console.log('error', error.message);
           showErrprMsg(error.message);
         });

      }
      else {

        console.log("invalid");
      }

    };
    $scope.goToForgotPasswd = function () {

      $location.path('/app/forgotpassword');

    };

    $scope.signUp = function () {

      $location.path('/app/signupstep1');
    };

    function showErrprMsg(msg) {
      $ionicPopup.alert({
        title: msg,
      })
    };

  });
