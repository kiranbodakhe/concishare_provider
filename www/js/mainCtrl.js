/**
 * Created by synerzip on 22/9/17.
 */
angular.module('concishare_provider')

.controller('mainCtrl', function ($scope, $http, $location) {

  if(localStorage.getItem('preference') === null || localStorage.getItem('preference') === 'undefined'){
    $http.get(env.devURl+'/papi/preferences').then(function(response){
      //the response from the server is now contained in 'response'
      localStorage.setItem("preference", JSON.stringify(response.data));
    }, function(error){
      console.log('error',error);
    });
  }
  
  $scope.goToLogin = function () {
    $location.path('/app/login');
  }
});
