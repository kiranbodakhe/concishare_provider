/**
 * Created by synerzip on 22/9/17.
 */
angular.module('concishare_provider')
  .controller('signUpCtrl',function ($scope, $http, $rootScope,$location, $ionicScrollDelegate, $ionicPopup) {
    console.log("Signupctrl");

    $scope.referralCode = '';
    $scope.enterCode = true;

    $scope.personalDetailsForm = true;
    $scope.cardDetailsForm = false;

    $rootScope.refcode = "";

    $scope.details = {

      name: "",
      email:"",
      phoneNumber:"",
      password:"",
      description:"",
      address:{
        address1:"",
        address2:"",
        city:"",
        zip:"",
        country:"",
        state:""

      },
      referralCodeUsed:"",
      yearsOfService:""
    };

    $scope.baseForm = {'fields':
      [
        {"fieldId": "name",
          "name": "Name *",
          "pattern": "^([a-zA-Z]+[.]?[ ]?|[a-zA-Z]+?)+$",
          "required": "true",
          "type": "string",
          "options": { "debounce": 0 }
        },
        { "fieldId": "phone",
          "name": "(123) 123-1234 *",
          "required": "true",
          "type": "string",
          "options": { "debounce": 0 }
        },
        { "fieldId": "email",
          "name": "Email *",
          "required": "true",
          "type": "string",
          "options": { "debounce": 0 }
        },
        { "fieldId": "password",
          "name": "Password *",
          "required": "true",
          "type": "string",
          "options": { "debounce": 0 }
        },
        {
          "fieldId": "address1",
          "name": "Address1 *",
          "required": "true",
          "type": "string",
          "options": { "debounce": 0 }
        },
        {
          "fieldId": "address2",
          "name": "Address2",
          "required": "address1",
          "type": "string",
          "options": { "debounce": 0 }
        },
        {
          "fieldId": "city",
          "name": "City *",
          "required": "true",
          "type": "string",
          "pattern": "^([a-zA-Z]+[.]?[ ]?|[a-zA-Z]+?)+$",
          "options": { "debounce": 0 }
        },
        {
          "fieldId": "zip",
          "name": "Zip *",
          "required": "true",
          "pattern": "^[0-9]{5}(?:-[0-9]{4})?$",
          "type": "string",
          "options": { "debounce": 0 }
        },
        {
          "fieldId": "state",
          "name": "State *",
          "required": "true",
          "pattern": "^([a-zA-Z]+[.]?[ ]?|[a-zA-Z]+?)+$",
          "type": "string",
          "options": { "debounce": 0 }
        },
        {
          "fieldId": "country",
          "name": "Country *",
          "required": "true",
          "pattern": "^([a-zA-Z]+[.]?[ ]?|[a-zA-Z]+?)+$",
          "type": "string",
          "options": { "debounce": 0 }
        },
        {
          "fieldId": "yearsOfService",
          "name": "Years of Service *",
          "required": "true",
          "type": "number",
          "options": { "debounce": 0 }
        },
        {
          "fieldId": "description",
          "name": "About me *",
          "required": "yearsOfService",
          "type": "string",
          "options": { "debounce": 0 }
        }
      ]
    };

    $scope.nextPage = function (signupStep1) {
      if(signupStep1.$valid){

        $ionicScrollDelegate.scrollTop();

        $scope.personalDetailsForm = false;
        $scope.cardDetailsForm = true;

      }
      else {

        console.log("invalid");
      }
    };

    $scope.addProvider = function(details) {
      $ionicScrollDelegate.scrollTop();

      if(angular.equals($scope.refCode,undefined)){

        $scope.refCode = "";

      }

      //for reference
      /*console.log("name",$scope.details.name);
      console.log("name from html",details.name);*/
      /*if (angular.equals(undefined, $scope.Name)) {
        $scope.Name = "Unknown";
      }*/

      var data = {
        name:               details.name,
        email :             details.email.toString(),
        phoneNumber:        details.phoneNumber.toString(),
        password:           details.password,
        yearsOfService:     details.experience,
        description:            details.description,
        address:{
          address1:           details.address1,
          address2:           details.address2,
          city:               details.city,
          zip:                details.zip.toString(),
          country:            details.country,
          state:              details.state,

        },
        referralCodeUsed:   $scope.refCode.toString()
      };

      var salt = gensalt(10);
      details.password = hashpw(details.password, salt, function(hash) {
        details.password = hash;
        $scope.details = details;

      });

      $http.post(env.devURl + '/papi/providers', data).success(function(data, status, headers, config) {
        console.log('Provider added.',data);
        $rootScope.authorizationData = data;
        $location.path('/add/login');

      }).error(function(error) {
        console.log('error', error.message);
        showErrprMsg(error.message);
      });
    };

    $scope.back = function () {
      $scope.personalDetailsForm = true;
      $scope.cardDetailsForm = false;
    };

    $scope.showPrompt = function() {
      $scope.enterCode = true;
      $ionicPopup.prompt({
        inputType: 'text',
        inputPlaceholder: 'Enter Referral Code',
        defaultText: $scope.referralCode,
        okText: 'Submit'

      }).then(function(res) {
        $scope.referralCode = res;
        if(res != '' && res != undefined){
          $scope.enterCode = false;
        }
      });
    };

    function showErrprMsg(msg) {
      $ionicPopup.alert({
        title: msg,
      })
    };
  });
