/**
 * Created by synerzip on 26/9/17.
 */
angular.module('concishare_provider')
  .controller('forgotPasswdCtrl',function ($scope, $http, $location) {

    $scope.baseForm = {'fields':
      [
        { "fieldId": "email",
          "name": "Enter your email *",
          "required": "true",
          "type": "string",
          "options": { "debounce": 0 }
        }
      ]
    };

    $scope.sendMail = function (emailId,forgotPasswordForm) {
      if(forgotPasswordForm.$valid){
        
        var data = {
          email: emailId
        };
        
        $http.post(env.devURl + '/papi/providers/generatePassword', data).success(function(data, status, headers, config) {
          $location.path('/app/resetpassword');

        }).error(function(data, status, headers, config) {
          console.log('mail sending failed.');
        });
      }
      else {

        console.log("invalid mail id");
      }
    };

    $scope.goToSignUp = function () {

      $location.path('/app/signupstep1');
    }
  });
