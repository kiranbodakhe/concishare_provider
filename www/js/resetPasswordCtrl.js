/**
 * Created by synerzip on 1/10/17.
 */
angular.module('concishare_provider')
  .controller('resetPasswordCtrl',function ($scope, $http,$location) {

    $scope.goToLogin = function () {

      $location.path('/app/login');

    }
  });

